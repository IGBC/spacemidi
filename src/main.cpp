#include "ui.h"

#include <cstdint>
#include <iostream>
#include <cmath>

#include <memory>
#include <spnav.h>
#include <jack/jack.h>
#include <jack/midiport.h>

MyWindow* window;
jack_port_t *outputPort;
constexpr uint8_t MIDICC = 0xB0; 

uint8_t map_range(int x) {
    return std::round(((x + 350.0) / 700.0) * 127.0);
}

int jack_process(jack_nframes_t nframes, void* args) {
    void* outputPortBuf(jack_port_get_buffer(outputPort, nframes));
    jack_midi_clear_buffer(outputPortBuf);

    spnav_event evt;
    size_t is_event(0);
    
    spnav_remove_events(SPNAV_EVENT_BUTTON);
    // get the last event, if there is any.
    while (spnav_poll_event(&evt) != 0)
        is_event++;

    if (is_event > 0) {
        auto m = evt.motion;

        uint8_t channel = window->get_channel();

        channel = MIDICC | (channel & 0xF);

        int ccs[6] = {window->get_cc_x(), window->get_cc_y(), window->get_cc_z(), 
                          window->get_cc_rx(), window->get_cc_ry(), window->get_cc_rz()};


        uint8_t values[6] = {map_range(m.x), map_range(m.y), map_range(m.z), 
                             map_range(m.rx), map_range(m.ry), map_range(m.rz)};

        for(size_t i = 0; i < 6; i++) {
            uint8_t data[3] = {channel, (uint8_t)ccs[i], values[i]};
            if (jack_midi_event_write(outputPortBuf, 0, data, 3) != 0)
                std::cerr << "buffer overflow";
        }
    }

    return 0;
}

void throw_error(Glib::RefPtr<Gtk::Application>& app, const char* msg) {
    Gtk::MessageDialog dialog(msg, false, Gtk::MessageType::ERROR);
    dialog.set_title("SpaceMidi Error");
    dialog.signal_response().connect([&](int r){app->quit();});
    dialog.present();
    app->signal_startup().connect([&](){app->add_window((Gtk::Window&)dialog);});
    app->run();
}

int main(int argc, char* argv[]) {
    auto app = Gtk::Application::create("tech.sigsegv.jack.spacemidi");
    
    if (spnav_open() != 0) {
        throw_error(app, "Could not connect to spacenav deamon, please ensure it is running.");
        return -1;
    }

    jack_status_t status;

    auto client(jack_client_open("Spacemidi 6DoF Input", JackNoStartServer, &status));
    if ((status & JackFailure) != 0) {
        throw_error(app, "Could not connect to JACK, please ensure the JACK server is running.");
        return -2;
    }

    outputPort = jack_port_register (client, "controller_out", JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);

    MyWindow win;
    win.show();
    window = &win;

    jack_set_process_callback(client, jack_process, nullptr);

    if (jack_activate(client) != 0) {
        throw_error(app, "Could not activate JACK callback, try restarting the server.");
        return -3;
    }

    app->signal_startup().connect([&](){app->add_window((Gtk::Window&)win);});
    app->run();

    spnav_close();
    return 0;
}