#include "ui.h"
#include <gtkmm-3.0/gtkmm/enums.h>

constexpr auto c_sarcasticRemarks = "<small><i>\"Six degrees of freedom should be\nenough for anyone\" - SEGFAULT</i></small>";

MyWindow::MyWindow() : 
m_channel(Gtk::Adjustment::create(1, 1, 16)),
m_label_channel("MIDI Channel", Gtk::Align::END),
m_hbox_channel(Gtk::Orientation::HORIZONTAL,5),
m_spin_channel(m_channel, 1.0, 0),

m_vbox_main(Gtk::Orientation::VERTICAL,25),
m_vbox_ccs(Gtk::Orientation::VERTICAL,5),

m_label_x("X Translation CC", Gtk::Align::END),
m_cc_x(Gtk::Adjustment::create(20, 0, 121)),
m_hbox_x(Gtk::Orientation::HORIZONTAL, 5),
m_spin_x(m_cc_x, 1.0, 0),

m_label_y("Y Translation CC", Gtk::Align::END),
m_cc_y(Gtk::Adjustment::create(21, 0, 121)),
m_hbox_y(Gtk::Orientation::HORIZONTAL, 5),
m_spin_y(m_cc_y, 1.0, 0),

m_label_z("Z Translation CC", Gtk::Align::END),
m_cc_z(Gtk::Adjustment::create(22, 0, 121)),
m_hbox_z(Gtk::Orientation::HORIZONTAL, 5),
m_spin_z(m_cc_z, 1.0, 0),

m_label_rx("X Rotation CC", Gtk::Align::END),
m_cc_rx(Gtk::Adjustment::create(23, 0, 121)),
m_hbox_rx(Gtk::Orientation::HORIZONTAL, 5),
m_spin_rx(m_cc_rx, 1.0, 0),

m_label_ry("Y Rotation CC", Gtk::Align::END),
m_cc_ry(Gtk::Adjustment::create(24, 0, 121)),
m_hbox_ry(Gtk::Orientation::HORIZONTAL, 5),
m_spin_ry(m_cc_ry, 1.0, 0),

m_label_rz("Z Rotation CC", Gtk::Align::END),
m_cc_rz(Gtk::Adjustment::create(25, 0, 121)),
m_hbox_rz(Gtk::Orientation::HORIZONTAL, 5),
m_spin_rz(m_cc_rz, 1.0, 0),

m_label_sarcasm()
{
  set_title("SpaceMidi Settings");
  //set_default_size(200, 200);
  set_child(m_vbox_main);

  m_vbox_main.set_margin(25);
  m_vbox_main.set_halign(Gtk::Align::CENTER);
  
  m_hbox_channel.append(m_label_channel);
  m_hbox_channel.append(m_spin_channel);
  m_spin_channel.set_hexpand(true);
  m_label_channel.set_hexpand(true);
  m_hbox_channel.set_halign(Gtk::Align::FILL);
  m_hbox_channel.set_homogeneous(true);
  m_vbox_main.append(m_hbox_channel);
  
  m_hbox_x.append(m_label_x);
  m_hbox_x.append(m_spin_x);
  m_spin_x.set_hexpand(true);
  m_label_x.set_hexpand(true);
  m_hbox_x.set_halign(Gtk::Align::FILL);
  m_hbox_x.set_homogeneous(true);
  m_vbox_ccs.append(m_hbox_x);

  m_hbox_y.append(m_label_y);
  m_hbox_y.append(m_spin_y);
  m_spin_y.set_hexpand(true);
  m_label_y.set_hexpand(true);
  m_hbox_y.set_halign(Gtk::Align::FILL);
  m_hbox_y.set_homogeneous(true);
  m_vbox_ccs.append(m_hbox_y);

  m_hbox_z.append(m_label_z);
  m_hbox_z.append(m_spin_z);
  m_spin_z.set_hexpand(true);
  m_label_z.set_hexpand(true);
  m_hbox_z.set_halign(Gtk::Align::FILL);
  m_hbox_z.set_homogeneous(true);
  m_vbox_ccs.append(m_hbox_z);

  m_hbox_rx.append(m_label_rx);
  m_hbox_rx.append(m_spin_rx);
  m_spin_rx.set_hexpand(true);
  m_label_rx.set_hexpand(true);
  m_hbox_rx.set_halign(Gtk::Align::FILL);
  m_hbox_rx.set_homogeneous(true);
  m_vbox_ccs.append(m_hbox_rx);
 
  m_hbox_ry.append(m_label_ry);
  m_hbox_ry.append(m_spin_ry);
  m_spin_ry.set_hexpand(true);
  m_label_ry.set_hexpand(true);
  m_hbox_ry.set_halign(Gtk::Align::FILL);
  m_hbox_ry.set_homogeneous(true);
  m_vbox_ccs.append(m_hbox_ry);

  m_hbox_rz.append(m_label_rz);
  m_hbox_rz.append(m_spin_rz);
  m_spin_rz.set_hexpand(true);
  m_label_rz.set_hexpand(true);
  m_hbox_rz.set_halign(Gtk::Align::FILL);
  m_hbox_rz.set_homogeneous(true);
  m_vbox_ccs.append(m_hbox_rz);

  m_label_sarcasm.set_markup(c_sarcasticRemarks);
  m_label_sarcasm.set_justify(Gtk::Justification::CENTER);
  m_vbox_ccs.append(m_label_sarcasm);

  m_vbox_main.append(m_vbox_ccs);

}

int MyWindow::get_channel() {
    return std::round(m_channel->get_value()) - 1;
}

int MyWindow::get_cc_x() {
    return std::round(m_cc_x->get_value());
}

int MyWindow::get_cc_y() {
    return std::round(m_cc_y->get_value());
}

int MyWindow::get_cc_z() {
    return std::round(m_cc_z->get_value());
}

int MyWindow::get_cc_rx() {
    return std::round(m_cc_rx->get_value());
}

int MyWindow::get_cc_ry() {
    return std::round(m_cc_ry->get_value());
}

int MyWindow::get_cc_rz() {
    return std::round(m_cc_rz->get_value());
}
