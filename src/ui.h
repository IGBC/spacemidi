#pragma once
#include <gtkmm.h>

class MyWindow : public Gtk::Window
{
public:
  MyWindow();
  Glib::RefPtr<Gtk::Adjustment> m_channel;
  Glib::RefPtr<Gtk::Adjustment> m_cc_x, m_cc_y, m_cc_z, m_cc_rx, m_cc_ry, m_cc_rz;

  int get_channel();
  int get_cc_x();
  int get_cc_y();
  int get_cc_z();
  int get_cc_rx();
  int get_cc_ry();
  int get_cc_rz();
  

private:  
  Gtk::Label m_label_channel, m_label_sarcasm;
  Gtk::SpinButton m_spin_channel;
  Gtk::Box m_vbox_main, m_vbox_ccs, m_hbox_channel;
  
  Gtk::Box m_hbox_x, m_hbox_y, m_hbox_z, m_hbox_rx, m_hbox_ry, m_hbox_rz; 
  Gtk::Label m_label_x, m_label_y, m_label_z, m_label_rx, m_label_ry, m_label_rz; 
  Gtk::SpinButton m_spin_x, m_spin_y, m_spin_z, m_spin_rx, m_spin_ry, m_spin_rz; 
};