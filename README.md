# SpaceMidi

Midi translator for [spacenav](http://spacenav.sourceforge.net/index.html) compatible 6DoF controllers. SpaceMidi translates each axis of your controller into a Midi CC value which can be used to modulate parameters in your synth.

![Screenshot of UI](ui.png)

## Building
Spacemidi requires the following packages
  * Jack Audio Connection Kit. (Sould also build with pipewire's implementation)
  * libspnav
  * gtkmm4
  * C++20 compatible compiler

Please also ensure the relevant development headers for your system are installed.

## Installation
There are currently no packages available.

## Usage
Ensure that both spacenavd and jackd (or pipewire pro audio) are running before launching the application. Spacemidi requires access to the display to function correctly.

## License
This software is licensed under GNU General Public License Version 3 or at your option any later version.

## Project status
This is a gadget I threw together in 2 days because I had one of thise space mouse devices lying around. If you use it and want a feature or find a bug please file it on the tracker or email me at segfault@mailbox.org
